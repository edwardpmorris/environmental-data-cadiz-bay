# Meterological and tidal elevation data for Cadiz Bay, SW Spain

Location: [Cádiz Bay, Spain](https://en.wikipedia.org/wiki/Bay_of_C%C3%A1diz)

Coordinates: 36.5420,-6.2496

## TODO:

+ [] add links to data sources
+ [] add code for querying data sources
+ [] add code for parsing data files
+ [] add docker image to run code and deploy figures to website

## Data sources

+ Copernicus atmospheric monitoring service (CAMS)
    + [MERRA (temperature, humidity, wind-speed)](http://www.soda-pro.com/web-services/meteo-data/merra)
        + manual access free, wps access paid
    + [CAMS-radiation-service (downwelling solar radiation)](http://www.soda-pro.com/web-services/radiation/cams-radiation-service)
        + manual access free, wps access free
+ Climate Prediction Center's ([CPC](www.cpc.ncep.noaa.gov)) rainfall data for the world (1979 to present, 50 km resolution)
    + access via [raincpc](https://cran.rstudio.com/web/packages/raincpc/index.html)

## R package list

+ [raincpc](https://cran.rstudio.com/web/packages/raincpc/index.html)
+ rwunderground
+ stationaRy

    