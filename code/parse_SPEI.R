# Parse SPEI from SPEIbase http://sac.csic.es/spei/database.html
# 1901−2014

parseSPEI <- function(file.path, start = "1901-01-15", return.zoo=T){
  spei <- readr::read_csv(file.path, skip = 1)
  tmstp <- seq.POSIXt(from=as.POSIXct(start, tz='UTC'), by ="month", length.out = nrow(spei))
  if(return.zoo==T){ return(zoo(spei, tmstp)) } else { return (data.frame(timestamp=tmstp, spei)) }
}

  
  