# Parse MERRA2 data file

# Parse a merra2 data file and return a dataframe with column names that match standard names in 'unitlabels' 

## HEADER
#Provider;National Aeronautics and Space Administration (NASA) / Goddard Space Flight Center
#More information at;http://gmao.gsfc.nasa.gov/reanalysis/MERRA-2
#Site latitude (positive means North);36.542
#Site longitude (positive means East);-6.250
#Site altitude (m);36
#Date beginning;1980-01-01
#Date end;2016-11-30
#Time reference (decimal hour);UT
#Summarization (period of integration);15 minutes (min)
#Date - Instant;The date or instant given for each value corresponds to the end of the summarization
#Temperature;Temperature at 2 m above ground in K
#Relative humidity;Relative humidity at 2 m above ground in %
#Pressure;Pressure at ground level in hPa
#Wind speed;Wind speed at 10 m above ground in m/s
#Wind direction;Wind direction at 10 m above ground in degrees (0 means from North, 90 from East...)
#Rainfall;Rainfall in kg/m2 (Rain depth in cm can be obtained by dividing the values by 10)
#Snowfall;Snowfall in kg/m2
#Snow depth;Snow depth in m

parseMerra <- function(file.path){
  dat <- readr::read_delim(file.path,";", escape_double = FALSE, col_types = cols(X13 = col_skip()), 
                                    trim_ws = TRUE, skip = 21, na = c("", "NA", -999))
  timestamp = lubridate::ymd(paste(dat$Year, dat$Month, dat$Day), tz = 'UTC') + (3600* dat$`UT time`)
  tdiff <- unique(as.numeric(diff(timestamp), units = "secs"))
  out <- data.frame(
    timestamp = timestamp,
    Tair.degC = dat$Temperature - 273.15, # deg. C
    RH = dat$`Relative Humidity`/100, # proportion
    apgl.hPa = dat$Pressure, # hPa
    u10.m_s = dat$`Wind speed`, # m/s
    u10dir.deg = dat$`Wind direction`, # degrees (N=0)
    precipRate.kg_m2s = dat$Rainfall/tdiff #Kg/m2s
    )
  out <- out[-nrow(out),]
  return(out)
}
