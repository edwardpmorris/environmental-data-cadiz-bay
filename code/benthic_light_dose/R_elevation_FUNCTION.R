# elevation {tripEstimation}  R Documentation
# Calculate elevation of astronomical objects
# 
# Description
# 
# Function to calculate elevation.
# 
# Usage
# 
# elevation(lon, lat, sun)
# Arguments
# 
# lon	
# vector of longitude values
# 
# lat	
# vector of latitude values
# 
# sun	
# pre-stored values as returned by solar or lunar
# 
# Author(s)
# 
# Michael D. Sumner
# 
# References
# 
# http://www.srrb.noaa.gov/highlights/sunrise/azel.html

elevation <- function (lon, lat, sun) 
{
    hourAngle <- sun$solarTime + lon - 180
    cosZenith <- (sin(pi/180 * lat) * sun$sinSolarDec + cos(pi/180 * 
        lat) * sun$cosSolarDec * cos(pi/180 * hourAngle))
    cosZenith[cosZenith > 1] <- 1
    cosZenith[cosZenith < -1] <- -1
    90 - 180/pi * acos(cosZenith)
}
